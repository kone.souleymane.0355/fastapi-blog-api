from sqlalchemy.orm import Session
from blog_field import models, schemas
from fastapi import HTTPException,status

def get_all(db: Session,curent_user: schemas.User):
    blogs = db.query(models.Blog).all()
    return blogs

def create_blog(request:schemas.Blog,db: Session):
    new_blog = models.Blog(**request.dict(),user_id=1)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog

def get_blog(id:int,db: Session):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()

    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Blog with the id: {id} does not exist")
    
    return blog


def update_blog(id:int,request: schemas.Blog, db: Session):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Blog with the id: {id} does not exist")
    blog.title = request.title
    blog.body = request.body
    db.commit()
    db.refresh(blog)
    return blog


def delete_blog(id:int, db: Session):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Blog with the id: {id} does not exist")
    db.delete(blog)
    db.commit()
    return {"message": "Blog deleted"}
