from sqlalchemy.orm import Session
from blog_field import models, schemas
from blog_field.hash import hash
from fastapi import HTTPException,status

def get_user(id:int, db: Session):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"User with the id: {id} does not exist")
    return user


def create_user(User: schemas.User, db:Session):
    User.password = hash(User.password)    
    new_user = models.User(**User.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def delete_user(id:int, db: Session):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User with the id: {id} does not exist")
    db.delete(user)
    db.commit()
    return {"message": "User deleted"}


def update_user(id:int, User: schemas.User, db:Session):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User with the id: {id} does not exist")
    if User.password:
        user.password = hash(User.password)
    if User.username:
        user.username = User.username
    if User.email:
        user.email = User.email
    db.commit()
    db.refresh(user)
    return user
