from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from sqlalchemy.orm import Session
from blog_field import schemas, databases
from blog_field.oauth2 import get_current_user
from repository import blog_repository as repo

get_db = databases.get_db

router = APIRouter()

'''
API for blogs
'''
@router.get("/blogs",response_model = List[schemas.BlogResponse])
def all(db: Session = Depends(get_db), curent_user: schemas.User = Depends(get_current_user)):
    return repo.get_all(db, curent_user)


@router.post("/blogs", status_code=status.HTTP_201_CREATED,response_model = schemas.BlogResponse)
def create(request: schemas.Blog, db: Session = Depends(get_db), curent_user: schemas.User = Depends(get_current_user)):
    return repo.create_blog(request,db)

    

@router.get("/blogs/{blog_id}", response_model = schemas.BlogResponse)
def show(blog_id: int, db: Session = Depends(get_db), curent_user: schemas.User = Depends(get_current_user)):
    return repo.get_blog(blog_id,db)
    


@router.put("/blogs/{blog_id}",status_code=status.HTTP_202_ACCEPTED)
def update(id: int, request: schemas.Blog, db: Session = Depends(get_db), curent_user: schemas.User = Depends(get_current_user)):
    return repo.update_blog(id,request,db)


@router.delete("/blogs/{blog_id}",status_code=status.HTTP_204_NO_CONTENT)
def delete(blog_id: int, db: Session = Depends(get_db), curent_user: schemas.User = Depends(get_current_user)):
   return repo.delete_blog(blog_id,db)