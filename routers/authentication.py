from operator import mod
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from blog_field import schemas, databases, models, hash
import JWTtoken

router = APIRouter()
get_db = databases.get_db

@router.post("/login")
def login(request: OAuth2PasswordRequestForm = Depends() ,db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.email == request.username).first()
    if not user:
        raise HTTPException(status_code= status.HTTP_404_NOT_FOUND, detail="Invalid credentials")
    if not hash.verify_password(request.password, user.password):
        raise HTTPException(status_code= status.HTTP_404_NOT_FOUND, detail="Invalid credentials")
    #Generate a jwt an return
    access_token_expires = JWTtoken.timedelta(minutes=JWTtoken.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = JWTtoken.create_access_token(
        data={"sub": user.email},
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "bearer"}