from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session
from blog_field import databases, schemas, models
from repository import user_repository as repo

get_db = databases.get_db

router = APIRouter()


'''
API for blogs Users
'''
@router.post("/users", status_code=status.HTTP_201_CREATED, response_model=schemas.UserResponse)
def create(User: schemas.User, db: Session = Depends(get_db)):
    return repo.create_user(User,db)


@router.get("/users/{user_id}", status_code=status.HTTP_200_OK, response_model=schemas.UserResponse)
def get_user(user_id:int, db: Session = Depends(get_db)):
    return repo.get_user(user_id,db)


@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(user_id: int, db: Session = Depends(get_user)) -> None:
    return repo.delete_user(user_id,db)


@router.put("/users/{user_id}", status_code=status.HTTP_202_ACCEPTED)
def update_user(user_id: int, User: schemas.User, db: Session = Depends(get_user)) -> None:
    return repo.update_user(user_id, User, db)