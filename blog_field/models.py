from sqlalchemy import Column, Integer, String, ForeignKey
from blog_field.databases import Base
from sqlalchemy.orm import relationship

'''
Model for blogs
'''
class Blog(Base):
    __tablename__ = 'blogs'
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(50), nullable=False)
    body = Column(String(500), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    creator = relationship('User', back_populates='blogs')


'''
Model for users
'''
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False)
    password = Column(String(255), nullable=False)

    blogs = relationship('Blog', back_populates="creator")