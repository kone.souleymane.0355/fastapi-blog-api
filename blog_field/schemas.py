from typing import List, Optional
from pydantic import BaseModel

'''
Schema for blogs
'''
class BlogBase(BaseModel):
    title: str
    body: str

class Blog(BlogBase):
    class Config:
        orm_mode = True

class Login(BaseModel):
    email:str
    password:str
    

'''
Schema for users
'''
class User(BaseModel):
    username: str
    email: str
    password: str

'''
User API response model(DTO)
'''
class UserResponse(BaseModel):
    username:str
    email:str
    blogs: List[Blog] = []
    class Config:
        orm_mode = True

'''
Blog API response model(DTO)
'''
class BlogResponse(Blog):
    title:str
    body:str
    creator: UserResponse
    class Config:
        orm_mode = True

'''
JWT schemas
'''
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None

