from typing import List
from fastapi import FastAPI
from blog_field import models,databases
from routers import authentication, blog_route, user_route



models.Base.metadata.create_all(bind=databases.engine)

app = FastAPI()

app.include_router(authentication.router, tags=["Authentication"])

app.include_router(blog_route.router, tags=['Blog'])

app.include_router(user_route.router, tags=['User'])
